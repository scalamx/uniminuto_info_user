#!/bin/bash

directorio="$( cd "$( dirname "$0" )" && pwd )/"

echo "Iniciando la maquina virtual..."
source "$directorio"infouser-env/bin/activate

echo "Ejecutando la aplicacion"
python3 "$directorio"main.py -a all

echo "Apagando la maquina virutal..."
deactivate
