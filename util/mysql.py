# -*- coding: utf-8 -*-
import pymysql
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from config.settings import DATABASES


class Conexion:
    def __init__(self, base_datos='default', bd_dev=None):
        try:
            self.__base_datos = base_datos
            self.__host = DATABASES[self.__base_datos]['HOST']
            self.__usuario = DATABASES[self.__base_datos]['USER']
            self.__puerto = int(DATABASES[self.__base_datos]['PORT']) if DATABASES[self.__base_datos]['PORT'] else \
                self._static['BD']['BD_PORT_DEFAULT']
            if bd_dev:
                self.__nombre = bd_dev
            else:
                self.__nombre = DATABASES[self.__base_datos]['NAME']
            self.__contrasena = DATABASES[self.__base_datos]['PASSWORD']
        except Exception:
            raise Exception("Error al establecer la conexion")

    def __conexion(self):
        _conexion = None
        try:
            _conexion = pymysql.connect(host=self.__host, user=self.__usuario,
                                        passwd=self.__contrasena, db=self.__nombre,
                                        port=self.__puerto)
        except pymysql.Error as error:
            raise Exception("Error al establecer la conexion")
        return _conexion

    def ejecutar(self, sql):
        _conexion = self.__conexion()
        _cursor = _conexion.cursor()
        try:
            _cursor.execute('SET NAMES utf8;')
            _cursor.execute('SET CHARACTER SET utf8;')
            _cursor.execute('SET character_set_connection=utf8;')
            _cursor.execute(sql)
        except Exception:
            raise Exception("Error al establecer la conexion")
        finally:
            _conexion.autocommit(True)
            _conexion.close()
        return True

    def create_cursor(self):
        try:
            _conexion = self.__conexion()
        except pymysql.Error:
            raise Exception("Error al establecer la conexion")
        return _conexion

    def create_engine(self):
        try:
            # sqlalchemy engine
            conexion = create_engine(URL(
                drivername="mysql+pymysql", username=self.__usuario, password=self.__contrasena, host=self.__host,
                database=self.__nombre, port=self.__puerto), pool_recycle=3600)
            engine = conexion.connect()
        except pymysql.Error:
            raise Exception("Error al establecer la conexion")
        return engine
