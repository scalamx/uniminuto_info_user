import pandas as pd

from config.settings import CHUNKSIZE
from config.sql import DATOS_AULA
from util.messages import print_message
from util.mysql import Conexion


class ConsultarAulaMoodle:

    def __init__(self, aula):
        self._aula = aula
        self._nombre_aula = 'moodle_aula_%s' % aula

    def consultar_informacion_aula(self, df_id_cursos):
        df_informacion_aula = pd.DataFrame()
        print_message("Obteniendo informacion del aula: " + str(self._nombre_aula))
        _conexion_aula_moddle = Conexion(base_datos=self._nombre_aula)
        cursor = _conexion_aula_moddle.create_cursor()
        list_id_cursos = list(df_id_cursos['courseid'])
        list_id_cursos.sort()
        idcursos_string = ",".join(map(str, list_id_cursos))
        consulta_aula = DATOS_AULA % (self._aula, idcursos_string)
        for chunk in pd.read_sql(sql=consulta_aula, con=cursor,chunksize=CHUNKSIZE):
            df_informacion_aula = pd.concat([df_informacion_aula, chunk])
        df_informacion_aula.drop_duplicates(inplace=True)
        df_informacion_aula.reset_index(drop=True, inplace=True)
        return df_informacion_aula
