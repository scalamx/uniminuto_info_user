import pandas as pd

from config.settings import CHUNKSIZE
from config.sql import CURSOS_ENROLLMENT
from util.messages import print_message
from util.mysql import Conexion


class ConsultarEnrollment:

    def __init__(self, aula):
        self._aula = aula
        self._nombre_aula = 'moodle_aula_%s' % aula

    def consultar_cursos_enrollment(self):
        df_cursos_aula = pd.DataFrame()
        print_message("Obteniendo lista de cursos del aula: " + str(self._nombre_aula))
        _conexion_enrollment = Conexion()
        cursor = _conexion_enrollment.create_cursor()
        for chunk in pd.read_sql(sql=CURSOS_ENROLLMENT % self._aula, con=cursor, chunksize=CHUNKSIZE):
            df_cursos_aula = pd.concat([df_cursos_aula, chunk])
        return df_cursos_aula
