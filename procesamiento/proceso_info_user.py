from config.settings import TABLA_INFO_USER, CHUNKSIZE
from config.sql import DELETE_INFO_USER
from util.messages import print_message
from util.mysql import Conexion


class OperacionesInfoUser:

    def __init__(self, aula):
        self._aula = aula
        self._nombre_aula = 'moodle_aula_%s' % aula
        self._conexion = Conexion()

    def eliminar_datos_info_user(self):
        print_message("Eliminando de la tabla info_user la informacion del aula: " + str(self._nombre_aula))
        resultado = self._conexion.ejecutar(DELETE_INFO_USER % (TABLA_INFO_USER, self._aula))
        if resultado:
            print_message("Registros eliminados...")
        else:
            print_message("Hubo un error al eliminar los registros...")

    def insertar_informacion_info_user(self, df_informacion_aula):
        if len(df_informacion_aula.index) > 0:
            print_message("Insertando los registros de: " + str(self._nombre_aula) + " en la tabla: " + TABLA_INFO_USER)
            engine = self._conexion.create_engine()
            print_message("El total de registros del aula a insertar es: " + str(df_informacion_aula.shape[0]))
            for chunk in self.chunker(df_informacion_aula, CHUNKSIZE):
                chunk.to_sql(name=TABLA_INFO_USER, con=engine, if_exists="append", index=False, method="multi")
            print_message("Registros insertados correctamente...")
        else:
            print_message("No se encontraron registros para insertar...")

    @staticmethod
    def chunker(seq, size):
        return (seq[pos: pos + size] for pos in range(0, len(seq), size))
