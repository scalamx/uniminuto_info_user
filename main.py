# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os

from config.settings import LISTA_AULAS, ARCHIVO_LOG
from procesamiento.consultar_aula import ConsultarAulaMoodle
from procesamiento.consultar_enrollment import ConsultarEnrollment
from procesamiento.proceso_info_user import OperacionesInfoUser
from util.messages import print_message


def inicio_proceso():
    print_message('Iniciando el proceso...')
    for _aula in LISTA_AULAS:
        print_message('Iniciando el proceso para el aula: ' + str(_aula))
        consultar_enrollment = ConsultarEnrollment(aula=_aula)
        df_cursos_aula = consultar_enrollment.consultar_cursos_enrollment()
        consultar_aula = ConsultarAulaMoodle(aula=_aula)
        df_informacion_aula = consultar_aula.consultar_informacion_aula(df_id_cursos=df_cursos_aula)
        info_user = OperacionesInfoUser(aula=_aula)
        info_user.eliminar_datos_info_user()
        info_user.insertar_informacion_info_user(df_informacion_aula=df_informacion_aula)
        print_message('Termino el proceso para el aula: ' + str(_aula))
    print_message('Termino el proceso de llenado de la tabla info_user')


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if os.path.exists(ARCHIVO_LOG):
        os.remove(ARCHIVO_LOG)
    inicio_proceso()