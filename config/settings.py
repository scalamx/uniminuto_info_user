# -*- coding: utf-8 -*-
import os
from os.path import join, dirname
from dotenv import load_dotenv, find_dotenv

dotenv_path = join(dirname(__file__), '../.env')
load_dotenv(find_dotenv())

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
CHUNKSIZE = int(os.environ.get("CHUNK_SIZE"))
TABLA_INFO_USER = os.environ.get("TABLE_INFO_USER")
ARCHIVO_LOG = 'output_info_user.txt'
LISTA_AULAS = ['02']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get("DATABASE_DEFAULT_NAME"),
        'USER': os.environ.get("DATABASE_DEFAULT_USER"),
        'PASSWORD': os.environ.get("DATABASE_DEFAULT_PASSWORD"),
        'HOST': os.environ.get("DATABASE_DEFAULT_HOST"),
        'PORT': os.environ.get("DATABASE_COMMON_PORT_NO_SECURITY"),
    },
    'moodle_aula_01': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get("DATABASE_AULA1_NAME"),
        'USER': os.environ.get("DATABASE_AULA1_USER"),
        'PASSWORD': os.environ.get("DATABASE_AULA1_PASSWORD"),
        'HOST': os.environ.get("DATABASE_AULA1_HOST"),
        'PORT': os.environ.get("DATABASE_AULA1_PORT"),
    },

    'moodle_aula_02': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get("DATABASE_AULA2_NAME"),
        'USER': os.environ.get("DATABASE_AULA2_USER"),
        'PASSWORD': os.environ.get("DATABASE_AULA2_PASSWORD"),
        'HOST': os.environ.get("DATABASE_AULA2_HOST"),
        'PORT': os.environ.get("DATABASE_AULA2_PORT"),
    },
}
