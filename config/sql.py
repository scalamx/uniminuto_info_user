CURSOS_ENROLLMENT = """
SELECT
distinct courseid
FROM enrolment
WHERE idAula = %s
ORDER BY
idAula, courseid
"""

DATOS_AULA = """
SELECT
mdl_user.username AS username,
mdl_course.id as courseid,
mdl_role.archetype as rol,
%s as idAula,
mdl_course.startdate as startdate
FROM mdl_user
INNER JOIN mdl_user_enrolments ON mdl_user.id = mdl_user_enrolments.userid
INNER JOIN mdl_enrol ON mdl_enrol.id = mdl_user_enrolments.enrolid
INNER JOIN mdl_course ON mdl_course.id = mdl_enrol.courseid
INNER JOIN mdl_context ON mdl_context.instanceid = mdl_course.id AND mdl_context.contextlevel = 50
INNER JOIN mdl_role_assignments ON mdl_role_assignments.contextid = mdl_context.id AND mdl_role_assignments.userid = mdl_user.id
INNER JOIN mdl_role ON mdl_role.id = mdl_role_assignments.roleid
WHERE mdl_user_enrolments.status = 0
AND mdl_course.visible = 1
AND	mdl_user.deleted = 0
AND mdl_user.suspended = 0
AND mdl_course.id in (%s)
"""

DELETE_INFO_USER = """
DELETE FROM %s WHERE idAula = %s
"""